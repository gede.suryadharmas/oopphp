<?php

require_once 'animal.php';
require_once 'ape.php';
require_once 'frog.php';

$sheep = new animal('shaun');
echo $sheep ->name . "<br>";
echo $sheep ->legs . "<br>";
echo var_dump($sheep->cold_blooded) . "<br>" . "<br>";

$sungokong = new ape('kera sakti'); // tak pernah berhenti bertindak sesuka hati
echo $sungokong -> get_name() . "<br>";
echo $sungokong ->legs;
echo $sungokong -> get_yell() . '<br>' . '<br>';

$kodok = new frog('buduk');
echo $kodok -> get_name() . "<br>";
echo $kodok ->legs;
echo $kodok -> get_jump() . '<br>';


?>
